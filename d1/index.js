let declaredFunction = function(){
    console.log("declaredFunction");
}

function animeList(){
    console.log("Recommended animes:")
    console.log("Naruto");
    console.log("Slam dunk");
    console.log("Bleach");
};

function movieList(){
    console.log("Recommended movies:")
    console.log("The Lost City");
    console.log("Aquaman");
    console.log("Shazam");
};

animeList();
movieList();

// we can also reassign declared functions and function expressions to new anonymous function
declaredFunction = function(){
    console.log("updated declaredFunction");
}
declaredFunction();

//however, we cannot change the declared functions/function expression that are defined using const.

const constFunction = function(){
    console.log("Initialized const function");
};
constFunction();

// constFunction = function(){
//     console.log("cannot be re-assigned");
// };
// constFunction();

// Function scoping 
{
    let localVar = "Armando Perez";
}
let globalVar = "Mr. Worldwide";

console.log(globalVar);
// console.log(localVar);

function newFunction(){
    let name = "jane";

    function nestedFunction(){
        let nestedName = "john";
        console.log(name);
    };
    console.log(name);
    nestedFunction();
};
newFunction();

//global scope variable
let globalName = "Alex";

function myNewFunction(){
    let nameInside = "Renz";

    console.log(globalName);
    console.log(nameInside);

}
myNewFunction();

// function showSampleAlert(){
//     alert("Hello Again!");
// }
// showSampleAlert();

// let samplePrompt = prompt("enter your name: ");
// console.log("hello "+ samplePrompt);

// let nullPrompt = prompt("Do not enter anything here");

// console.log(nullPrompt);

// function welcome(){
//     let userFirstName = prompt("Input first name.");
//     let userLastName = prompt("Input last name.");
//     console.log("Hello "+ userFirstName+ " "+ userLastName);

// }
// welcome();

// 1. definitive of the task that it will perform
// 2. name you function with small letters, incase of multipe words, use camelCasing (underscore is also valid)

function getCourses(){
    let courses = ["Science 101","Math 101","English 101"];
    console.log(courses);
}
getCourses();
function get(){
    let name = "jamie"
    console.log(name);
}
get();
function foo(){
    console.log(25%2);
}
foo();